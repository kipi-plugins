msgid ""
msgstr ""
"Project-Id-Version: kipiplugin_flickrexport\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2018-01-15 09:49+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-doc@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-IgnoreConsistency: delete\n"
"X-POFile-IgnoreConsistency: write\n"
"X-POFile-SpellExtra: xxx Frob RPC Kipi POST Flickr SOAP FlickrUploadr\n"
"X-POFile-SpellExtra: photoSets flick Caulier Gilles Vardhman Jain Renko hq\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Luka zooomr Zooomr Photostream Shourya Gupta Singh\n"

#: comboboxintermediate.h:54
#, kde-format
msgid "Various"
msgstr "Vários"

#: flickrlist.cpp:420
#, kde-format
msgid ""
"Check if photo should be publicly visible or use Upload Options tab to "
"specify this for all images"
msgstr ""
"Assinalar se a fotografia deverá estar pública ou use a página de Opções de "
"Envio para indicar isto para todas as imagens"

#: flickrlist.cpp:423
#, kde-format
msgid ""
"Check if photo should be visible to family or use Upload Options tab to "
"specify this for all images"
msgstr ""
"Assinalar se a fotografia deverá estar visível para a família ou use a "
"página de Opções de Envio para indicar isto para todas as imagens"

#: flickrlist.cpp:426
#, kde-format
msgid ""
"Check if photo should be visible to friends or use Upload Options tab to "
"specify this for all images"
msgstr ""
"Assinalar se a fotografia deverá estar visível para os amigos ou use a "
"página de Opções de Envio para indicar isto para todas as imagens"

#: flickrlist.cpp:429
#, kde-format
msgid ""
"Indicate the safety level for the photo or use Upload Options tab to specify "
"this for all images"
msgstr ""
"Indique o nível de segurança da fotografia ou use a página de Opções de "
"Envio para indicar isto para todas as imagens"

#: flickrlist.cpp:432
#, kde-format
msgid ""
"Indicate what kind of image this is or use Upload Options tab to specify "
"this for all images"
msgstr ""
"Indique o tipo desta imagem ou use a página de Opções de Envio para indicar "
"isto para todas as imagens"

#: flickrlist.cpp:445
#, kde-format
msgid ""
"Add extra tags per image or use Upload Options tab to add tags for all images"
msgstr ""
"Adicione as marcas extra por cada imagem  ou use a página de Opções de Envio "
"para indicar isto para todas as imagens"

#: flickrlist.cpp:462
#, kde-format
msgid "Enter extra tags, separated by commas."
msgstr "Indique as marcas extra, separadas por vírgulas."

#: flickrtalker.cpp:265
#, kde-format
msgid "Getting the maximum allowed file size."
msgstr "A obter o tamanho máximo dos ficheiros permitido."

#: flickrtalker.cpp:550
#, kde-format
msgid "File Size exceeds maximum allowed file size."
msgstr "O tamanho do ficheiro ultrapassa o tamanho máximo permitido."

#: flickrtalker.cpp:603
#, kde-format
msgid "No photo specified"
msgstr "Não está nenhuma foto seleccionada"

#: flickrtalker.cpp:607
#, kde-format
msgid "General upload failure"
msgstr "Erro geral no envio"

#: flickrtalker.cpp:611
#, kde-format
msgid "Filesize was zero"
msgstr "O tamanho do ficheiro era zero"

#: flickrtalker.cpp:615
#, kde-format
msgid "Filetype was not recognized"
msgstr "O tipo de ficheiro não foi reconhecido"

#: flickrtalker.cpp:619
#, kde-format
msgid "User exceeded upload limit"
msgstr "O utilizador ultrapassou o limite de envios"

#: flickrtalker.cpp:623
#, kde-format
msgid "Invalid signature"
msgstr "A assinatura é inválida"

#: flickrtalker.cpp:627
#, kde-format
msgid "Missing signature"
msgstr "Falta a assinatura"

#: flickrtalker.cpp:631
#, kde-format
msgid "Login Failed / Invalid auth token"
msgstr "Erro na Autenticação / Resultado da autenticação inválido"

#: flickrtalker.cpp:635
#, kde-format
msgid "Invalid API Key"
msgstr "A Chave da API é Inválida"

#: flickrtalker.cpp:639
#, kde-format
msgid "Service currently unavailable"
msgstr "O serviço está indisponível de momento"

#: flickrtalker.cpp:643
#, kde-format
msgid "Invalid Frob"
msgstr "O Frob é Inválido"

#: flickrtalker.cpp:647
#, kde-format
msgid "Format \"xxx\" not found"
msgstr "O formato \"xxx\" não foi encontrado"

#: flickrtalker.cpp:651
#, kde-format
msgid "Method \"xxx\" not found"
msgstr "O método \"xxx\" não foi encontrado"

#: flickrtalker.cpp:655
#, kde-format
msgid "Invalid SOAP envelope"
msgstr "O envelope de SOAP é inválido"

#: flickrtalker.cpp:659
#, kde-format
msgid "Invalid XML-RPC Method Call"
msgstr "A Chamada do Método de XML-RPC é Inválida"

#: flickrtalker.cpp:663
#, kde-format
msgid "The POST method is now required for all setters"
msgstr "O método POST é agora obrigatório para todos os modificadores"

#: flickrtalker.cpp:667
#, kde-format
msgid "Unknown error"
msgstr "Erro desconhecido"

#: flickrtalker.cpp:672 flickrtalker.cpp:696 flickrtalker.cpp:852
#: flickrwindow.cpp:416
#, kde-format
msgid "Error"
msgstr "Erro"

#: flickrtalker.cpp:673
#, kde-format
msgid ""
"Error Occurred: %1\n"
"Cannot proceed any further."
msgstr ""
"Ocorreu um Erro: %1\n"
"Não é possível prosseguir a partir daqui."

#: flickrtalker.cpp:852
#, kde-format
msgid "PhotoSet creation failed: "
msgstr "Falhou a criação do conjunto de fotos: "

#: flickrtalker.cpp:947
#, kde-format
msgid "Failed to fetch list of photo sets."
msgstr "Não foi possível obter a lista de conjuntos de fotografias."

#: flickrtalker.cpp:1027
#, kde-format
msgid "Failed to upload photo"
msgstr "Não foi possível enviar a fotografia"

#: flickrtalker.cpp:1086
#, kde-format
msgid "Failed to query photo information"
msgstr "Não foi possível obter informações sobre a foto"

#: flickrwidget.cpp:63
#, kde-format
msgid "Remove Account"
msgstr "Remover a Conta"

#: flickrwidget.cpp:73 flickrwidget.cpp:77
#, kde-format
msgctxt "photo permissions"
msgid "Public"
msgstr "Pública"

#: flickrwidget.cpp:76
#, kde-format
msgid "This is the list of images to upload to your Flickr account."
msgstr "Esta é a lista de imagens a enviar para a sua conta do Flickr."

#: flickrwidget.cpp:81
#, kde-format
msgid "Extra tags"
msgstr "Marcas extra"

#: flickrwidget.cpp:87 flickrwidget.cpp:98
#, kde-format
msgctxt "photo permissions"
msgid "Family"
msgstr "Família"

#: flickrwidget.cpp:92 flickrwidget.cpp:100
#, kde-format
msgctxt "photo permissions"
msgid "Friends"
msgstr "Amigos"

#: flickrwidget.cpp:107
#, kde-format
msgid "Safety level"
msgstr "Nível de segurança"

#: flickrwidget.cpp:109
#, kde-format
msgid "Content type"
msgstr "Tipo de conteúdo"

#: flickrwidget.cpp:112
#, kde-format
msgctxt "photo safety level"
msgid "Safe"
msgstr "Segura"

#: flickrwidget.cpp:113
#, kde-format
msgctxt "photo safety level"
msgid "Moderate"
msgstr "Moderada"

#: flickrwidget.cpp:114
#, kde-format
msgctxt "photo safety level"
msgid "Restricted"
msgstr "Restrita"

#: flickrwidget.cpp:115 flickrwidget.cpp:211
#, kde-format
msgctxt "photo content type"
msgid "Photo"
msgstr "Fotografia"

#: flickrwidget.cpp:116 flickrwidget.cpp:212
#, kde-format
msgctxt "photo content type"
msgid "Screenshot"
msgstr "Imagem"

#: flickrwidget.cpp:117 flickrwidget.cpp:213
#, kde-format
msgctxt "photo content type"
msgid "Other"
msgstr "Outra"

#: flickrwidget.cpp:134
#, kde-format
msgid "Tag options"
msgstr "Opções das marcas"

#: flickrwidget.cpp:138
#, kde-format
msgid "Use Host Application Tags"
msgstr "Usar as Marcas da Aplicação"

#: flickrwidget.cpp:140 flickrwidget.cpp:501
#, kde-format
msgid "More tag options"
msgstr "Mais opções das marcas"

#: flickrwidget.cpp:154
#, kde-format
msgid "Added Tags: "
msgstr "Marcas Adicionadas: "

#: flickrwidget.cpp:156
#, kde-format
msgid "Enter new tags here, separated by commas."
msgstr "Indique aqui as marcas novas, separadas por vírgulas."

#: flickrwidget.cpp:158
#, kde-format
msgid "Add tags per image"
msgstr "Adicionar as marcas por imagem"

#: flickrwidget.cpp:159
#, kde-format
msgid "If checked, you can set extra tags for each image in the File List tab"
msgstr ""
"Se estiver assinalada, poderá definir marcas extra por cada imagem, na "
"página da Lista de Ficheiros"

#: flickrwidget.cpp:163
#, kde-format
msgid "Strip Spaces From Tags"
msgstr "Retirar os Espaços das Marcas"

#: flickrwidget.cpp:176
#, kde-format
msgid "Publication Options"
msgstr "Opções de Publicação"

#: flickrwidget.cpp:181
#, kde-format
msgctxt "As in accessible for people"
msgid "Public (anyone can see them)"
msgstr "Público (todos as podem ver)"

#: flickrwidget.cpp:184
#, kde-format
msgid "Visible to Family"
msgstr "Visível para a Família"

#: flickrwidget.cpp:187
#, kde-format
msgid "Visible to Friends"
msgstr "Visível para os Amigos"

#: flickrwidget.cpp:190 flickrwidget.cpp:488
#, kde-format
msgid "More publication options"
msgstr "Mais opções de publicação"

#: flickrwidget.cpp:203
#, kde-format
msgid "Safety level:"
msgstr "Nível de segurança:"

#: flickrwidget.cpp:205
#, kde-format
msgid "Safe"
msgstr "Seguro"

#: flickrwidget.cpp:206
#, kde-format
msgid "Moderate"
msgstr "Moderado"

#: flickrwidget.cpp:207
#, kde-format
msgid "Restricted"
msgstr "Restrito"

#: flickrwidget.cpp:209
#, kde-format
msgid "Content type:"
msgstr "Tipo de conteúdo:"

#: flickrwidget.cpp:304
#, kde-format
msgid ""
"<b><h2><a href='http://www.23hq.com'><font color=\"#7CD164\">23</font></a> "
"Export</h2></b>"
msgstr ""
"<b><h2><a href='http://www.23hq.com'>Exportar para <font color="
"\"#7CD164\">23</font></a></h2></b>"

#: flickrwidget.cpp:311
#, kde-format
msgid ""
"<b><h2><a href='http://www.flickr.com'><font color=\"#0065DE\">flick</"
"font><font color=\"#FF0084\">r</font></a> Export</h2></b>"
msgstr ""
"<b><h2>Exportação para o <a href='http://www.flickr.com'><font color="
"\"#0065DE\">flick</font><font color=\"#FF0084\">r</font></a></h2></b>"

#: flickrwidget.cpp:484
#, kde-format
msgid "Fewer publication options"
msgstr "Menos opções de publicação"

#: flickrwidget.cpp:506
#, kde-format
msgid "Fewer tag options"
msgstr "Menos opções das marcas"

#: flickrwindow.cpp:71
#, kde-format
msgid "Export to %1 Web Service"
msgstr "Exportar para o Serviço Web %1"

#: flickrwindow.cpp:119
#, kde-format
msgid "Start Uploading"
msgstr "Iniciar o Envio"

#: flickrwindow.cpp:131
#, kde-format
msgid "Flickr/23 Export"
msgstr "Exportação para o Flickr/23"

#: flickrwindow.cpp:132
#, kde-format
msgid "A tool to export an image collection to a Flickr / 23 web service."
msgstr ""
"Uma ferramenta para exportar a colecção de imagens para o serviço Web "
"Flickr / 23."

#: flickrwindow.cpp:134
#, kde-format
msgid ""
"(c) 2005-2008, Vardhman Jain\n"
"(c) 2008-2015, Gilles Caulier\n"
"(c) 2009, Luka Renko\n"
"(c) 2015, Shourya Singh Gupta"
msgstr ""
"(c) 2005-2008, Vardhman Jain\n"
"(c) 2008-2015, Gilles Caulier\n"
"(c) 2009, Luka Renko\n"
"(c) 2015, Shourya Singh Gupta"

#: flickrwindow.cpp:139
#, kde-format
msgid "Vardhman Jain"
msgstr "Vardhman Jain"

#: flickrwindow.cpp:139
#, kde-format
msgid "Author and maintainer"
msgstr "Autoria e manutenção"

#: flickrwindow.cpp:142
#, kde-format
msgid "Gilles Caulier"
msgstr "Gilles Caulier"

#: flickrwindow.cpp:142 flickrwindow.cpp:145
#, kde-format
msgid "Developer"
msgstr "Desenvolvimento"

#: flickrwindow.cpp:145
#, kde-format
msgid "Shourya Singh Gupta"
msgstr "Shourya Singh Gupta"

#: flickrwindow.cpp:557
#, kde-format
msgid "Photostream Only"
msgstr "Apenas para o Photostream"

#: flickrwindow.cpp:741
#, kde-format
msgid "Flickr Export"
msgstr "Exportação para o Flickr"

#: flickrwindow.cpp:759
#, kde-format
msgid "Failed to Fetch Photoset information from %1. %2\n"
msgstr ""
"Não foi possível obter informações sobre o conjunto de fotos do %1. %2\n"

#: flickrwindow.cpp:765
#, kde-format
msgid "Warning"
msgstr "Aviso"

#: flickrwindow.cpp:766
#, kde-format
msgid ""
"Failed to upload photo into %1. %2\n"
"Do you want to continue?"
msgstr ""
"Não foi possível enviar a foto para o %1. %2\n"
"Deseja continuar?"

#: flickrwindow.cpp:769
#, kde-format
msgid "Continue"
msgstr "Continuar"

#: flickrwindow.cpp:770
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#. i18n: ectx: Menu (Export)
#: kipiplugin_flickrui.rc:7
#, kde-format
msgid "&Export"
msgstr "&Exportar"

#. i18n: ectx: ToolBar (mainToolBar)
#: kipiplugin_flickrui.rc:14
#, kde-format
msgid "Main Toolbar"
msgstr "Barra Principal"

#: plugin_flickr.cpp:100
#, kde-format
msgid "Export to Flick&r..."
msgstr "Exportar para o Flick&r..."

#: selectuserdlg.cpp:47
#, kde-format
msgid "Flickr Account Selector"
msgstr "Selector de Contas do Flickr"

#: selectuserdlg.cpp:52
#, kde-format
msgid "Add another account"
msgstr "Adicionar outra conta"

#: selectuserdlg.cpp:75
#, kde-format
msgid "Choose the %1 account to use for exporting images:"
msgstr "Escolher a conta %1 a usar para exportar as imagens:"
