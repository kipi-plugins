# Translation of kipiplugin_imgur.po to Catalan (Valencian)
# Copyright (C) 2013-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2013, 2014, 2015, 2017, 2020, 2023.
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: kipi-plugins\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2023-02-22 21:41+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.2\n"

#: imgurapi3.cpp:132
#, kde-format
msgid "Could not authorize"
msgstr "No s'ha pogut autoritzar"

#: imgurapi3.cpp:309
#, kde-format
msgid "Could not open file"
msgstr "No s'ha pogut obrir el fitxer"

#: imgurimageslist.cpp:59
#, kde-format
msgid "Thumbnail"
msgstr "Miniatura"

#: imgurimageslist.cpp:62
#, kde-format
msgid "Submission title"
msgstr "Presenta el títol"

#: imgurimageslist.cpp:65
#, kde-format
msgid "Submission description"
msgstr "Presenta la descripció"

#: imgurimageslist.cpp:68
#, kde-format
msgid "Imgur URL"
msgstr "URL d'Imgur"

#: imgurimageslist.cpp:71
#, kde-format
msgid "Imgur Delete URL"
msgstr "Suprimix l'URL d'Imgur"

#: imgurwindow.cpp:82
#, kde-format
msgid "Logged in as:"
msgstr "Connectat com a:"

#: imgurwindow.cpp:90
#, kde-format
msgid "Forget"
msgstr "Oblida"

#: imgurwindow.cpp:100
#, kde-format
msgid "Upload Anonymously"
msgstr "Puja anònimament"

#: imgurwindow.cpp:116
#, kde-format
msgid "Export to imgur.com"
msgstr "Exporta cap a imgur.com"

#: imgurwindow.cpp:119
#, kde-format
msgid "Upload"
msgstr "Puja"

#: imgurwindow.cpp:120
#, kde-format
msgid "Start upload to Imgur"
msgstr "Comença la pujada a Imgur"

#: imgurwindow.cpp:125
#, kde-format
msgid "Imgur Export"
msgstr "Exportador cap a Imgur"

#: imgurwindow.cpp:126
#, kde-format
msgid "A tool to export images to Imgur web service"
msgstr "Una eina per a exportar imatges al servei web d'Imgur"

#: imgurwindow.cpp:127
#, kde-format
msgid "(c) 2012-2013, Marius Orcsik"
msgstr "(c) 2012-2013, Marius Orcsik"

#: imgurwindow.cpp:129
#, kde-format
msgid "Marius Orcsik"
msgstr "Marius Orcsik"

#: imgurwindow.cpp:130
#, kde-format
msgid "Author"
msgstr "Autoria"

#: imgurwindow.cpp:133
#, kde-format
msgid "Gilles Caulier"
msgstr "Gilles Caulier"

#: imgurwindow.cpp:134 imgurwindow.cpp:138
#, kde-format
msgid "Developer"
msgstr "Desenvolupador"

#: imgurwindow.cpp:137
#, kde-format
msgid "Fabian Vogt"
msgstr "Fabian Vogt"

#: imgurwindow.cpp:220
#, kde-format
msgid "<Not logged in>"
msgstr "<No connectat>"

#: imgurwindow.cpp:227
#, kde-format
msgid "Authorization Failed"
msgstr "No s'ha pogut fer l'autorització"

#: imgurwindow.cpp:228
#, kde-format
msgid "Failed to log into Imgur: %1\n"
msgstr "No s'ha pogut connectar-se a Imgur: %1\n"

#: imgurwindow.cpp:254 imgurwindow.cpp:261
#, kde-format
msgid "Uploading Failed"
msgstr "Ha fallat la pujada"

#: imgurwindow.cpp:255
#, kde-format
msgid "Failed to upload photo to Imgur: %1\n"
msgstr "No s'ha pogut pujar la fotografia a Imgur: %1\n"

#: imgurwindow.cpp:262
#, kde-format
msgid ""
"Failed to upload photo to Imgur: %1\n"
"Do you want to continue?"
msgstr ""
"No s'ha pogut pujar la fotografia a Imgur: %1\n"
"Voleu continuar?"

#. i18n: ectx: Menu (Export)
#: kipiplugin_imgurui.rc:7
#, kde-format
msgid "&Export"
msgstr "&Exporta"

#. i18n: ectx: ToolBar (mainToolBar)
#: kipiplugin_imgurui.rc:13
#, kde-format
msgid "Main Toolbar"
msgstr "Barra d'eines principal"

#: plugin_imgur.cpp:102
#, kde-format
msgid "Export to &Imgur..."
msgstr "Exporta cap a &Imgur…"
